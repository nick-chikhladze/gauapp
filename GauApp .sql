-- phpMyAdmin SQL Dump
-- version 4.7.3
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Ноя 25 2018 г., 23:30
-- Версия сервера: 5.7.19
-- Версия PHP: 7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `GauApp`
--

-- --------------------------------------------------------

--
-- Структура таблицы `appendence`
--

CREATE TABLE `appendence` (
  `id` int(11) NOT NULL,
  `studentPin` int(11) NOT NULL,
  `subject` int(11) NOT NULL,
  `weekday` int(11) NOT NULL,
  `I` tinyint(1) NOT NULL,
  `II` tinyint(1) NOT NULL,
  `III` tinyint(1) NOT NULL,
  `IV` tinyint(1) NOT NULL,
  `V` tinyint(1) NOT NULL,
  `VI` tinyint(1) NOT NULL,
  `VII` tinyint(1) NOT NULL,
  `VIII` tinyint(1) NOT NULL,
  `IX` tinyint(1) NOT NULL,
  `X` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `Marks`
--

CREATE TABLE `Marks` (
  `id` int(11) NOT NULL,
  `studentPin` varchar(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `firstMidterm` int(2) NOT NULL,
  `secondMidterm` int(2) NOT NULL,
  `final` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `Marks`
--

INSERT INTO `Marks` (`id`, `studentPin`, `title`, `firstMidterm`, `secondMidterm`, `final`) VALUES
(1, '01011081819', 'Java Programming', 20, 20, 60),
(2, '12312312312', 'C#(ASP.NET)', 19, 18, 59);

-- --------------------------------------------------------

--
-- Структура таблицы `Users`
--

CREATE TABLE `Users` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `pin` varchar(11) NOT NULL,
  `password` varchar(50) NOT NULL,
  `birthday` date NOT NULL,
  `gender` tinyint(1) NOT NULL,
  `faculty` varchar(50) NOT NULL,
  `course` int(1) NOT NULL,
  `permission` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `Users`
--

INSERT INTO `Users` (`id`, `name`, `lastname`, `pin`, `password`, `birthday`, `gender`, `faculty`, `course`, `permission`) VALUES
(1, 'Nick', 'Chikhladze', '01011081819', 'root', '1998-05-06', 1, 'Informatics and Engineering', 3, 1),
(2, 'Mariam', 'Babisha', '12312312312', 'student', '1998-10-08', 0, 'Informatics and Engineering', 3, 0),
(3, 'Alexandree', 'Machavariani', '11111111111', 'student', '1998-10-08', 1, 'Law, Social Sciences and Diplomacy', 3, 0),
(4, 'Natia', 'Mestvirishvili', '11111111110', 'student', '1997-11-04', 0, 'Liberal Arts & Humanities', 4, 0),
(5, 'Qeti', 'Qetelauri', '11110111111', 'student', '1998-11-11', 0, 'Business', 2, 0),
(6, 'Ioane', 'Tandashvili', '01011121516', 'student', '2018-11-04', 1, 'Law, Social Sciences and Diplomacy', 2, 0),
(7, 'aaaa', 'asd', '1231231', 'asd', '2018-11-01', 1, 'Business', 1, 0);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `appendence`
--
ALTER TABLE `appendence`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `Marks`
--
ALTER TABLE `Marks`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `Users`
--
ALTER TABLE `Users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `pin` (`pin`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `appendence`
--
ALTER TABLE `appendence`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `Marks`
--
ALTER TABLE `Marks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `Users`
--
ALTER TABLE `Users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
