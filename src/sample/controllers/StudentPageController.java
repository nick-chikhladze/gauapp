package sample.controllers;

import java.net.URL;
import java.sql.ResultSet;
import java.util.ResourceBundle;

import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

public class StudentPageController extends LoginController {
    private ResultSet res;
    private ObservableList<User> list;
    private String[] loginInfo;

    public StudentPageController(){
        this.loginInfo = super.getLoginInfo();
    }

    @FXML    private ResourceBundle resources;
    @FXML    private URL location;
    @FXML    private Button table;
    @FXML    private Button attendance;
    @FXML    private Button point;
    @FXML    private TableView<User> tableVIew;
    @FXML    private TableColumn<User, Integer> subId;
    @FXML    private TableColumn<User, String> subName;
    @FXML    private TableColumn<User, Integer> subFMT;
    @FXML    private TableColumn<User, Integer> subSMT;
    @FXML    private TableColumn<User, Integer> subFinal;
    @FXML    private TableColumn<User, Integer> subTotal;
    @FXML
    void initialize() {
        subId.setCellValueFactory(new PropertyValueFactory<User, Integer>("subjectId"));
        subName.setCellValueFactory(new PropertyValueFactory<User, String>("subjectTitle"));
        subFMT.setCellValueFactory(new PropertyValueFactory<User, Integer>("subjectFMT"));
        subSMT.setCellValueFactory(new PropertyValueFactory<User, Integer>("subjectSMT"));
        subFinal.setCellValueFactory(new PropertyValueFactory<User, Integer>("subjectFinal"));
        subTotal.setCellValueFactory(new PropertyValueFactory<User, Integer>("total"));

        User newUser = new User(loginInfo[0],loginInfo[1]);
        tableData(newUser);

    }

    public void tableData(User user){
        //clear table before it gets data
        tableVIew.getItems().clear();

        DBHandler dbHandler = new DBHandler();
        res = dbHandler.getSubject(user);
        try {
            while (res.next()) {

                User newUser = new User();
                newUser.setSubjectId(this.res.getInt("id"));
                newUser.setPin(this.res.getString("studentPin"));
                newUser.setSubjectTitle(this.res.getString("title"));
                newUser.setSubjectFMT(this.res.getInt("firstMidterm"));
                newUser.setSubjectSMT(this.res.getInt("secondMidterm"));
                newUser.setSubjectFinal(this.res.getInt("final"));

                newUser.fillSubList();

                list = newUser.getList();

            }
        }catch(Exception ex){ ex.printStackTrace(); }

        tableVIew.setItems(list);
        tableVIew.refresh();
    }
}
