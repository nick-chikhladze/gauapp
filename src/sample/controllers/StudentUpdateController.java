package sample.controllers;

import java.net.URL;
import java.sql.ResultSet;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;

public class StudentUpdateController {
    private ResultSet getInfo;
    private boolean check = true;

    private String upd_id;
    private String upd_name;
    private String upd_lastname;
    private Date upd_bday;
    private String upd_pass;
    private String upd_pin;
    private Boolean upd_gender;
    private String upd_faculty;
    private int upd_course;

    ObservableList<String> facultylist = FXCollections.observableArrayList(new String[]{"Informatics and Engineering", "Law, Social Sciences and Diplomacy", "Business", "Liberal Arts & Humanities"});
    ObservableList<Integer> courselist = FXCollections.observableArrayList(1,2,3,4);
    ObservableList<String> genderlist = FXCollections.observableArrayList("Male","Female");

    @FXML   private ResourceBundle resources;
    @FXML   private URL location;
    @FXML   private TextField updName;
    @FXML   private TextField updLname;
    @FXML   private DatePicker updBday;
    @FXML   private ComboBox<String> updFaculty;
    @FXML   private ComboBox<Integer> updCourse;
    @FXML   private Button updBtn;
    @FXML   private ComboBox<Boolean> updPermission;
    @FXML   private TextField updIdnumber;
    @FXML   private ComboBox<String> updGender;
    @FXML   private TextField updPass;
    @FXML   private TextField updStudentid;

    @FXML
    private Button btnPreload;

    @FXML
    void initialize() {

        updFaculty.setItems(facultylist);
        updCourse.setItems(courselist);
        updGender.setItems(genderlist);

        btnPreload.setOnAction(event -> {

            if(updStudentid.getText() == null){
                System.out.println("Enter id number!");
            }else{
//                try{
//                    Integer.parseInt(updStudentid.getText());
//                }catch(NumberFormatException ex){
//                    System.out.println("Try again");
//                    check = false;
//                    closeWindow(btnPreload);
//                }

                if(check) {
                    try {

                        DBHandler db = new DBHandler();
                        User newUser = new User();

                        getInfo = db.getUserById(updStudentid.getText());
                        while(getInfo.next()) {

                            newUser.setId(getInfo.getInt("id"));
                            newUser.setName(getInfo.getString("name"));
                            newUser.setLastname(getInfo.getString("lastname"));
                            newUser.setPin(getInfo.getString("pin"));
                            newUser.setPassword(getInfo.getString("password"));
                            newUser.setBirthday(getInfo.getDate("birthday"));
                            newUser.setGender(getInfo.getBoolean("gender"));
                            newUser.setFaculty(getInfo.getString("faculty"));
                            newUser.setCourse(getInfo.getInt("course"));
                            newUser.setPermission(getInfo.getBoolean("permission"));

                        }

                        updIdnumber.clear();
                        updName.clear();
                        updPass.clear();
                        updLname.clear();
                        updStudentid.clear();

                        updStudentid.appendText(Integer.toString(newUser.getId()));
                        updName.appendText(newUser.getName());
                        updBday.setValue(LocalDate.of(1998, 10, 8));
                        updPass.appendText(newUser.getPassword());
                        updLname.appendText(newUser.getLastname());
                        updIdnumber.appendText(newUser.getPin());
                        updGender.setValue(newUser.getGender() ? "Male" : "Female");
                        updFaculty.setValue(newUser.getFaculty());
                        updCourse.setValue(newUser.getCourse());

                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

        });

        updBtn.setOnAction(event -> {
            if(updStudentid.getText() == null || updLname.getText() == null || updIdnumber.getText() == null || updPass.getText() == null || updBday.getValue() == null || updGender.getValue() == null || updFaculty.getValue()  == null || updCourse.getValue()  < 1){
                System.out.println("Try again and fill all fields!");
            }else{

                upd_id = updStudentid.getText();
                upd_name = updName.getText();
                upd_lastname = updLname.getText();
                upd_pin = updIdnumber.getText();
                upd_pass = updPass.getText();
                upd_bday = java.sql.Date.valueOf(updBday.getValue());
                upd_gender = updGender.getValue() == "Male" ? true : false;
                upd_faculty = updFaculty.getValue();
                upd_course = updCourse.getValue();

                try {

                    DBHandler db = new DBHandler();
                    User newUser = new User();

                    getInfo = db.getUserById(upd_id);
                    while(getInfo.next()) {

                        newUser.setId(getInfo.getInt("id"));
                        newUser.setName(upd_name);
                        newUser.setLastname(upd_lastname);
                        newUser.setPin(upd_pin);
                        newUser.setPassword(upd_pass);
                        newUser.setBirthday(upd_bday);
                        newUser.setGender(upd_gender);
                        newUser.setFaculty(upd_faculty);
                        newUser.setCourse(upd_course);
                        newUser.setPermission(getInfo.getBoolean("permission"));

                    }
                    db.updateUser(newUser);
                    closeWindow(updBtn);

                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        });

//                ArrayList<String> userArr = new ArrayList<>();
//                userArr.add(getInfo.getString("name"));
//                userArr.add(getInfo.getString("lastname"));
//                userArr.add(getInfo.getString("pin"));
//                userArr.add(getInfo.getString("password"));
//                userArr.add(getInfo.getDate("birthday").toString());
//                userArr.add(Boolean.toString(getInfo.getBoolean("gender")));
//                userArr.add(getInfo.getString("faculty"));
//                userArr.add(Integer.toString(getInfo.getInt("course")));



//                ArrayList<String> upd_arr = new ArrayList<>();
//                upd_arr.add(upd_name);
//                upd_arr.add(upd_lastname);
//                upd_arr.add(upd_pin);
//                upd_arr.add(upd_pass);
//                upd_arr.add(upd_bday.toString());
//                upd_arr.add(upd_gender.toString());
//                upd_arr.add(upd_faculty);
//                upd_arr.add(Integer.toString(upd_course));
//
//
//                ArrayList<Integer> index_arr = new ArrayList<>();
//                for(int i = 0;i < upd_arr.size();i++){
//                    if(upd_arr.get(i) == null){
//                        index_arr.add(i);
//                    }
//                }
//
//                for(int j = 0;j < index_arr.size();j++){
//                    upd_arr.set(index_arr.get(j), userArr.get(index_arr.get(j)));
//                }
    }

    public void closeWindow(Button btn){
        btn.getScene().getWindow().hide();
    }

}
