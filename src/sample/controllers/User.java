package sample.controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.util.Date;

public class User {

    private int id;
    private String name;
    private String lastname;
    private String pin;
    private String password;
    private Date birthday;
    private Boolean gender;
    private String faculty;
    private int course;
    private Boolean permission;

    private int subjectId;
    private String subjectTitle;
    private int subjectFMT = 0;
    private int subjectSMT = 0;
    private int subjectFinal = 0;
    private int subjectTotal = 0;

    private static ObservableList<User> list = FXCollections.observableArrayList();

    //login constructor
    public User(String pin_number, String password){
        this.pin = pin_number;
        this.password = password;
    }

    //full constructor
    public User(int user_id,String user_name, String user_lastname, String user_pin, String user_pass, Date user_bday, Boolean user_gender, String user_faculty, int user_course, Boolean user_permission) {
        this.id = user_id;
        this.name = user_name;
        this.lastname = user_lastname;
        this.pin = user_pin;
        this.password = user_pass;
        this.birthday = user_bday;
        this.gender = user_gender;
        this.faculty = user_faculty;
        this.course = user_course;
        this.permission = user_permission;
    }

    //no permission constructor
    public User(String user_name, String user_lastname, String user_pin, String user_pass, Date user_bday, Boolean user_gender, String user_faculty, int user_course) {
        this.name = user_name;
        this.lastname = user_lastname;
        this.pin = user_pin;
        this.password = user_pass;
        this.birthday = user_bday;
        this.gender = user_gender;
        this.faculty = user_faculty;
        this.course = user_course;
        this.permission = false;
    }

    //empty constructor
    public User() {

    }

    //subject constructor
    public User(int subId, String subName, int subFMT, int subSMT, int subFinal){
        this.subjectId = subId;
        this.subjectTitle = subName;
        this.subjectFMT = subFMT;
        this.subjectSMT = subSMT;
        this.subjectFinal = subFinal;
        setTotal();
        this.subjectTotal = getTotal();
    }

    //select constructor
    public User(int id, String user_pin, String user_pass, Boolean user_perm){
        this.id = id;
        this.pin = user_pin;
        this.password = user_pass;
        this.permission = user_perm;
    }

    public int getId(){ return id; }

    public void setId(int id){ this.id = id; }

    public String getName() {
        return name;
    }

    public void setName(String user_name) {
        this.name = user_name;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String user_lastname) {
        this.lastname = user_lastname;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String user_pin) {
        this.pin = user_pin;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String user_password) {
        this.password = user_password;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date user_bday) {
        this.birthday = user_bday;
    }

    public Boolean getGender() {
        return gender;
    }

    public void setGender(Boolean user_gender) {
        this.gender = user_gender;
    }

    public String getFaculty() {
        return faculty;
    }

    public void setFaculty(String user_faculty) {
        this.faculty = user_faculty;
    }

    public int getCourse() {
        return course;
    }

    public void setCourse(int user_course) {
        this.course = user_course;
    }

    public Boolean getPermission() {
        return permission;
    }

    public void setPermission(Boolean user_permission) { this.permission = user_permission; }



    public int getSubjectId(){ return subjectId; }

    public void setSubjectId(int subId) { this.subjectId = subId; }

    public String getSubjectTitle() { return subjectTitle; }

    public void setSubjectTitle(String subTitle) { this.subjectTitle = subTitle; }

    public int getSubjectFMT(){ return subjectFMT; }

    public void setSubjectFMT(int subFmt){ this.subjectFMT = subFmt; }

    public int getSubjectSMT(){ return subjectSMT; }

    public void setSubjectSMT(int subSmt){ this.subjectSMT = subSmt; }

    public int getSubjectFinal(){ return subjectFinal; }

    public void setSubjectFinal(int subFinal){ this.subjectFinal = subFinal; }



    public void fillList(){ list.add(new User(getId(),getName(),getLastname(),getPin(),getPassword(),getBirthday(),getGender(),getFaculty(),getCourse(),getPermission())); }

    public void fillSubList(){ list.add(new User(getSubjectId(),getSubjectTitle(),getSubjectFMT(),getSubjectSMT(),getSubjectFinal())); }



    public ObservableList<User> getList(){ return list; }

    public void setTotal(){ this.subjectTotal = this.subjectFMT + this.subjectSMT + this.subjectFinal; }

    public int getTotal(){ return subjectTotal; }

}