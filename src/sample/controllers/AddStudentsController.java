package sample.controllers;

import java.net.URL;
import java.util.Date;
import java.util.ResourceBundle;

import com.sun.org.apache.xpath.internal.operations.Bool;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;

public class AddStudentsController implements Initializable{

    @FXML    private TextField regName;
    @FXML    private TextField regLname;
    @FXML    private DatePicker regBday;
    @FXML    private TextField regId;
    @FXML    private TextField regPass;
    @FXML    private ComboBox<String> regGender;
    @FXML    private ComboBox<String> regFaculty;
    @FXML    private ComboBox<Integer> regCourse;
    @FXML    private ComboBox<?> regPermission;
    @FXML    private Button regButton;

    ObservableList<String> facultylist = FXCollections.observableArrayList(new String[]{"Informatics and Engineering", "Law, Social Sciences and Diplomacy", "Business", "Liberal Arts & Humanities"});
    ObservableList<Integer> courselist = FXCollections.observableArrayList(1,2,3,4);
    ObservableList<String> genderlist = FXCollections.observableArrayList("Male","Female");

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        regFaculty.setItems(facultylist);
        regCourse.setItems(courselist);
        regGender.setItems(genderlist);

        regButton.setOnAction(event -> {
            Boolean gender = regGender.getValue() == "Male" ? true : false;
            if(regName.getText().trim() == null || regLname.getText().trim() == null || regId.getText().trim() == null || regPass.getText().trim() == null || java.sql.Date.valueOf(regBday.getValue()) == null || gender == null || regFaculty.getValue() == null || regCourse.getValue() == null){
                System.out.println("Enter all values!");
            }else {
                User newUser = new User(regName.getText().trim(), regLname.getText().trim(), regId.getText().trim(), regPass.getText().trim(), java.sql.Date.valueOf(regBday.getValue()), gender, regFaculty.getValue(), regCourse.getValue());
                DBHandler dbHandler = new DBHandler();
                dbHandler.registerUser(newUser);
                closeWindow(regButton);
            }
        });
    }

    public void closeWindow(Button btn){
        btn.getScene().getWindow().hide();
    }
}

