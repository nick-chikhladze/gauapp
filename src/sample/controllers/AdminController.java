package sample.controllers;

import java.io.IOException;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.ResourceBundle;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

public class AdminController extends LoginController implements Initializable{
    private ResultSet res;
    private ObservableList<User> list;

    private String[] loginInfo;

    public AdminController(){
        this.loginInfo = super.getLoginInfo();

    }

    @FXML    Button btn1;
    @FXML    Button btn2;
    @FXML    private Button btnDel;
    @FXML    private ImageView img;
    @FXML    private ImageView icon1;
    @FXML    private ImageView icon2;
    @FXML    private ImageView icon3;
    @FXML    private ResourceBundle resources;
    @FXML    private URL location;
    @FXML    private TextField delId;
    @FXML    public  TableView<User> tableVIew;
    @FXML    private TableColumn<User, Integer> colId;
    @FXML    private TableColumn<User, String> colName;
    @FXML    private TableColumn<User, String> colLastname;
    @FXML    private TableColumn<User, String> colPin;
    @FXML    private TableColumn<User, String> colPass;
    @FXML    private TableColumn<User, Date> colBday;
    @FXML    private TableColumn<User, Boolean> colGender;
    @FXML    private TableColumn<User, String> colFaculty;
    @FXML    private TableColumn<User, Integer> colCourse;
    @FXML    private TableColumn<User, Boolean> colPermission;
    @FXML    private ComboBox<String> courseCB;
    @FXML    private ComboBox<String> facultyCB;

    //combobox information | NULL error
    ObservableList<String> facultylist = FXCollections.observableArrayList(new String[]{"Informatics and Engineering", "Law, Social Sciences and Diplomacy", "Business", "Liberal Arts & Humanities"});
    ObservableList<String> courselist = FXCollections.observableArrayList(new String[]{"1", "2", "3", "4"});


    @Override
    public void initialize(URL arg0, ResourceBundle arg1) {
        // TODO Auto-generated method stub

        this.courseCB.setItems(this.courselist);
        this.facultyCB.setItems(this.facultylist);

        colId.setCellValueFactory(new PropertyValueFactory<User, Integer>("id"));
        colName.setCellValueFactory(new PropertyValueFactory<User, String>("name"));
        colLastname.setCellValueFactory(new PropertyValueFactory<User, String>("lastname"));
        colPin.setCellValueFactory(new PropertyValueFactory<User, String>("pin"));
        colPass.setCellValueFactory(new PropertyValueFactory<User, String>("password"));
        colBday.setCellValueFactory(new PropertyValueFactory<User, Date>("birthday"));
        colGender.setCellValueFactory(new PropertyValueFactory<User, Boolean>("gender"));
        colFaculty.setCellValueFactory(new PropertyValueFactory<User, String>("faculty"));
        colCourse.setCellValueFactory(new PropertyValueFactory<User, Integer>("course"));
        colPermission.setCellValueFactory(new PropertyValueFactory<User, Boolean>("permission"));

        btn1.setOnAction(event -> {
            String course, faculty;
            String[] filter = new String[3];

            course = courseCB.getValue();
            faculty = facultyCB.getValue();

            if(course == null && faculty == null){
                filter[0] = "all";
                tableData(filter);
            }else if(course != null && faculty == null){
                filter[0] = "course";
                filter[1] = course;
                tableData(filter);
            }else if(course == null && faculty != null){
               filter[0] = "faculty";
               filter[2] = faculty;
               tableData(filter);
            }else{
                filter[0] = "both";
                filter[1] = course;
                filter[2] = faculty;
                tableData(filter);
            }
        });

        btnDel.setOnAction(event -> {
            String delUserId = delId.getText();
            DBHandler dbHandler = new DBHandler();
            dbHandler.deleteUser(delUserId);
        });
    }
    @FXML
    public void buttons(ActionEvent event) throws IOException{
//        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("../views/Students.fxml"));
//        Parent root1 = (Parent) fxmlLoader.load();
//        Stage stages = new Stage();
//        stages.setScene(new Scene(root1));
//        stages.show();
    }
    @FXML
    public void buttons1(ActionEvent event) throws IOException{
        FXMLLoader fxmlLoader1 = new FXMLLoader(getClass().getResource("../views/AddStudents.fxml"));
        Parent root = (Parent) fxmlLoader1.load();
        Stage stages = new Stage();
        stages.setTitle("Add Students");
        stages.setScene(new Scene(root));
        stages.show();
    }

    @FXML
    public void buttons3(ActionEvent event) throws IOException{
        FXMLLoader fxmlLoader1 = new FXMLLoader(getClass().getResource("../views/StudentUpdate.fxml"));
        Parent root = (Parent) fxmlLoader1.load();
        Stage stages = new Stage();
        stages.setTitle("Update Students");
        stages.setScene(new Scene(root));
        stages.show();
    }
    @FXML
    public void delete(ActionEvent event) throws IOException{
//        FXMLLoader fxmlLoader1 = new FXMLLoader(getClass().getResource("../views/Delete.fxml"));
//        Parent root = (Parent) fxmlLoader1.load();
//        Stage stages = new Stage();
//        stages.setTitle("Delete Student");
//        stages.setScene(new Scene(root));
//        stages.show();
//        String delUserId = delId.getText();
//        DBHandler dbHandler = new DBHandler();
//        dbHandler.deleteUser(delUserId);
    }
   
    public void tableData(String[] filter){
        //clear table before it gets data
                tableVIew.getItems().clear();

                User getInfo = new User(loginInfo[0], loginInfo[1]);
                DBHandler dbHandler = new DBHandler();
                res = dbHandler.getFullUser(getInfo, filter);
                try {
                    while (res.next()) {
                        User newUser = new User();
                        newUser.setId(this.res.getInt("id"));
                        newUser.setName(this.res.getString("name"));
                        newUser.setLastname(this.res.getString("lastname"));
                        newUser.setPin(this.res.getString("pin"));
                        newUser.setPassword(this.res.getString("password"));
                        newUser.setBirthday(this.res.getDate("birthday"));
                        newUser.setGender(this.res.getBoolean("gender"));
                        newUser.setFaculty(this.res.getString("faculty"));
                        newUser.setCourse(this.res.getInt("course"));
                        newUser.setPermission(this.res.getBoolean("permission"));
                        newUser.fillList();
                        list = newUser.getList();
                    }
                }catch(Exception ex){ ex.printStackTrace(); }

                tableVIew.setItems(list);
                tableVIew.refresh();
    }

}
