package sample.controllers;

import java.io.IOException;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.sun.org.apache.xpath.internal.operations.Bool;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class LoginController {
    private static String user_pin, user_pass;
    private Boolean user_permission;

    private boolean check = false;
    private ResultSet res;
    
    @FXML    private ResourceBundle resources;
    @FXML    private URL location;
    @FXML    private PasswordField password;
    @FXML    private TextField username;
    @FXML    private Button loginButton;
    @FXML    private Button errorbtn;

    @FXML
    void initialize() {


        loginButton.setOnMouseClicked((event) -> {

            this.user_pin = username.getText().trim();
            this.user_pass = password.getText().trim();

            DBHandler dbHandler = new DBHandler();
            try {
                dbHandler.getDbConnection();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } catch (SQLException e) {
                e.printStackTrace();
            }

            User checkUser = new User(user_pin, user_pass);
            String s = user_pin;
            String s1 = user_pass;
            
            if (s1.isEmpty() || s.isEmpty())
            {
				try {
					FXMLLoader fxmlLoader1 = new FXMLLoader(getClass().getResource("../views/error.fxml"));
	                Parent root2;
					root2 = (Parent) fxmlLoader1.load();
					Stage stages1 = new Stage();
					stages1.setTitle("error");
	                stages1.setScene(new Scene(root2));
	                stages1.show();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
            }
            res = dbHandler.getLogin(checkUser);
            try {
                while (res.next()) {
                    if(res.getString("pin").equals(user_pin) && res.getString("password").equals(user_pass)){
                        check = true;
                        user_permission = res.getBoolean("permission");
                        break;
                    }
                }
            }catch(Exception ex){ex.printStackTrace();}

            if(check){
                if(user_permission) {
                    closeWindow(loginButton);

                    try {
                        FXMLLoader fxmlLoader = new FXMLLoader();
                        fxmlLoader.setLocation(getClass().getResource("../views/AdminPage.fxml"));

                        Scene scene = new Scene(fxmlLoader.load());
                        Stage stage = new Stage();
                        stage.setTitle("Admin Page");
                        stage.setScene(scene);
                        stage.show();

                    } catch (IOException e) {
                        Logger logger = Logger.getLogger(getClass().getName());
                        logger.log(Level.SEVERE, "Failed to create new Window.", e);
                    }

                }else{

                        closeWindow(loginButton);

                        try {
                            FXMLLoader fxmlLoader = new FXMLLoader();
                            fxmlLoader.setLocation(getClass().getResource("../views/StudentPage.fxml"));

                            Scene scene = new Scene(fxmlLoader.load());
                            Stage stage = new Stage();
                            stage.setTitle("Student Page");
                            stage.setScene(scene);
                            stage.show();
                        } catch (IOException e) {
                            Logger logger = Logger.getLogger(getClass().getName());
                            logger.log(Level.SEVERE, "Failed to create new Window.", e);
                        }

                }
            }else
                System.out.println("Wrong data!");

//            if(this.user.equals("root")){
//                closeWindow(loginButton);
//
//                try {
//                    FXMLLoader fxmlLoader = new FXMLLoader();
//                    fxmlLoader.setLocation(getClass().getResource("../views/AdminPage.fxml"));
//
//                    Scene scene = new Scene(fxmlLoader.load());
//                    Stage stage = new Stage();
//                    stage.setTitle("Admin Page");
//                    stage.setScene(scene);
//                    stage.show();
//
//                } catch (IOException e) {
//                    Logger logger = Logger.getLogger(getClass().getName());
//                    logger.log(Level.SEVERE, "Failed to create new Window.", e);
//                }
//            }else if(this.user.equals("student")){
//                closeWindow(loginButton);
//
//                try {
//                    FXMLLoader fxmlLoader = new FXMLLoader();
//                    fxmlLoader.setLocation(getClass().getResource("../views/StudentPage.fxml"));
//
//                    Scene scene = new Scene(fxmlLoader.load());
//                    Stage stage = new Stage();
//                    stage.setTitle("Student Page");
//                    stage.setScene(scene);
//                    stage.show();
//                } catch (IOException e) {
//                    Logger logger = Logger.getLogger(getClass().getName());
//                    logger.log(Level.SEVERE, "Failed to create new Window.", e);
//                }
//            }
        });


    }

    public String[] getLoginInfo(){
        String[] returnArr = new String[]{this.user_pin,this.user_pass};
        return returnArr;
    }

    public void closeWindow(Button btn){
        btn.getScene().getWindow().hide();
    }

}

