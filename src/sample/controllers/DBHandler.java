package sample.controllers;

import java.sql.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;
//import java.util.TimeZone;

import sample.Configs.Config;
import sample.Configs.Constants;

public class DBHandler extends Config {
    Connection dbConnection;

    public Connection getDbConnection() throws ClassNotFoundException, SQLException {
        String connectionString = "jdbc:mysql://" + dbHost + ":" + dbPort + "/" + dbName;
        Class.forName("com.mysql.jdbc.Driver");

        dbConnection = DriverManager.getConnection(connectionString, dbUser, dbPass);

        return dbConnection;
    }

    public ResultSet getFullUser(User user, String[] filter) {
        ResultSet resSet = null;

        switch (filter[0]){
            case "all":
                try {
                    Statement stmt = null;
                    stmt = getDbConnection().createStatement();
                    String sql = "SELECT * FROM Users";
                    resSet = stmt.executeQuery(sql);

                } catch (SQLException e) {
                    e.printStackTrace();
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }
                break;
            case "course":
                try {

                    Statement stmt = null;
                    stmt = getDbConnection().createStatement();
                    String sql = "SELECT * FROM Users WHERE course = " + String.valueOf(filter[1]);
                    resSet = stmt.executeQuery(sql);

                } catch (SQLException e) {
                    e.printStackTrace();
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }
                break;
            case "faculty":
                try {

                    Statement stmt = null;
                    stmt = getDbConnection().createStatement();
                    String sql = "SELECT * FROM Users WHERE faculty = '" + filter[2] + "'";
                    resSet = stmt.executeQuery(sql);

                } catch (SQLException e) {
                    e.printStackTrace();
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }
                break;
            case "both":
                try {

                    Statement stmt = null;
                    stmt = getDbConnection().createStatement();
                    String sql = "SELECT * FROM Users WHERE course = " + filter[1] + " AND faculty = '" + filter[2] + "'";
                    resSet = stmt.executeQuery(sql);

                } catch (SQLException e) {
                    e.printStackTrace();
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }
                break;

        }


//        try {
//
//            Statement stmt = null;
//            stmt = getDbConnection().createStatement();
//            String sql = "SELECT * FROM Users";
//            resSet = stmt.executeQuery(sql);
//
//        } catch (SQLException e) {
//            e.printStackTrace();
//        } catch (ClassNotFoundException e) {
//            e.printStackTrace();
//        }

        return resSet;
    }

    public ResultSet getUserById(String user_id) {
        ResultSet resSet = null;

        try {
            Statement stmt = null;
            stmt = getDbConnection().createStatement();
            String sql = "SELECT * FROM Users WHERE id = " + String.valueOf(user_id);
            resSet = stmt.executeQuery(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }


        return resSet;
    }

    public ResultSet getLogin(User user){
        ResultSet resSet = null;

        try {

            Statement stmt = null;
            stmt = getDbConnection().createStatement();
            String sql = "SELECT " + Constants.USER_PIN + "," + Constants.USER_PASSWORD + "," + Constants.USER_PERMISSION + " FROM Users";
            resSet = stmt.executeQuery(sql);

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        return resSet;
    }

    public void registerUser(User user){

        String insert = "INSERT INTO " +
                    Constants.USER_TABLE +
                    "(" +
                    Constants.USER_NAME + "," +
                    Constants.USER_LASTNAME + "," +
                    Constants.USER_PIN + "," +
                    Constants.USER_PASSWORD + "," +
                    Constants.USER_DATE + "," +
                    Constants.USER_GENDER + "," +
                    Constants.USER_FACULTY + "," +
                    Constants.USER_COURSE + "," +
                    Constants.USER_PERMISSION +
                    ")" +
                    "VALUES(?,?,?,?,?,?,?,?,?)";

            try {
                PreparedStatement prSt = getDbConnection().prepareStatement(insert);
                prSt.setString(1, user.getName());
                prSt.setString(2, user.getLastname());
                prSt.setString(3, user.getPin());
                prSt.setString(4, user.getPassword());
                prSt.setDate(5, (Date) user.getBirthday());
                prSt.setBoolean(6, user.getGender());
                prSt.setString(7, user.getFaculty());
                prSt.setInt(8, user.getCourse());
                prSt.setBoolean(9, false);

                prSt.executeUpdate();
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }

    }

    public void updateUser(User user){

        String update = "UPDATE " +
                    Constants.USER_TABLE + " SET name = ? , lastname = ? , pin = ? , password = ? , birthday = ? , " +
                    "gender = ? , faculty = ? , course = ? WHERE id = ?";

            try {
                PreparedStatement prSt = getDbConnection().prepareStatement(update);
                prSt.setString(1, user.getName());
                prSt.setString(2, user.getLastname());
                prSt.setString(3, user.getPin());
                prSt.setString(4, user.getPassword());
                prSt.setDate(5, (Date) user.getBirthday());
                prSt.setBoolean(6, user.getGender());
                prSt.setString(7, user.getFaculty());
                prSt.setInt(8, user.getCourse());
                prSt.setInt(9, user.getId());

//                System.out.println(prSt);


                prSt.executeUpdate();
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }


    }

    //remake on boolean
    public void deleteUser(String user_id){
        try {
            Statement stmt = null;
            stmt = getDbConnection().createStatement();
            String sql = "DELETE FROM Users WHERE id = " + String.valueOf(user_id);
            stmt.execute(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public ResultSet getSubject(User user){
        ResultSet resSet = null;

        try {
            Statement stmt = null;
            stmt = getDbConnection().createStatement();
            String sql = "SELECT * FROM Marks WHERE studentPin = " + String.valueOf(user.getPin());
            resSet = stmt.executeQuery(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }


        return resSet;
    }
}
