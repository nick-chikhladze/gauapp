package sample.Configs;

public class Constants {

        public static final String USER_TABLE      = "Users";

        public static final String USER_ID         = "id";
        public static final String USER_NAME       = "name";
        public static final String USER_LASTNAME   = "lastname";
        public static final String USER_PIN        = "pin";
        public static final String USER_PASSWORD   = "password";
        public static final String USER_DATE       = "birthday";
        public static final String USER_GENDER     = "gender";
        public static final String USER_FACULTY    = "faculty";
        public static final String USER_COURSE     = "course";
        public static final String USER_PERMISSION = "permission";


}


