-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 26, 2018 at 02:17 AM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gauapp`
--

-- --------------------------------------------------------

--
-- Table structure for table `appendence`
--

CREATE TABLE `appendence` (
  `id` int(11) NOT NULL,
  `studentPin` int(11) NOT NULL,
  `subject` int(11) NOT NULL,
  `weekday` int(11) NOT NULL,
  `I` tinyint(1) NOT NULL,
  `II` tinyint(1) NOT NULL,
  `III` tinyint(1) NOT NULL,
  `IV` tinyint(1) NOT NULL,
  `V` tinyint(1) NOT NULL,
  `VI` tinyint(1) NOT NULL,
  `VII` tinyint(1) NOT NULL,
  `VIII` tinyint(1) NOT NULL,
  `IX` tinyint(1) NOT NULL,
  `X` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `biznesi`
--

CREATE TABLE `biznesi` (
  `id` int(11) NOT NULL,
  `lector` varchar(100) NOT NULL,
  `subject` varchar(100) NOT NULL,
  `score` int(3) NOT NULL,
  `course` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `biznesi`
--

INSERT INTO `biznesi` (`id`, `lector`, `subject`, `score`, `course`) VALUES
(1, 'ნიკოლოზ ჩიხლაძე', 'ბიზნესის საფუძვლები', 30, 1),
(2, 'ლუკა ქუთათელაძე', 'სტარტაპი', 30, 1),
(3, 'ლერი სოხაძე', 'ბიზნეს კომუნიკაცია', 38, 1),
(4, 'ნათია მესტვირიშვილი', 'წუწუნინგი', 0, 1),
(5, 'ლუკა ქუთათელაძე', 'სტარტაპი 2', 0, 2),
(6, 'ნიკოლოზ ჩიხლაძე', 'ბიზნესის საფუძვლები 2', 0, 2),
(7, 'ლერი სოხაძე', 'ბიზნეს კომუნიკაცია 2', 0, 2),
(8, 'ნათია მესტვირიშვილი', 'წუწუნინგ 2', 0, 2),
(9, 'ნათია მესტვირიშვილი ', 'წუწუნინგი(გაღრმავებული)', 1, 3),
(10, 'ლერი სოხაძე', 'ბიზნეს საფუძვლები(გაღრმავებული)', 22, 3),
(11, 'ლუკა ქუთათელაძე', 'სტარტაპი(გაღრმავებული)', 20, 3),
(12, 'ნიკოლოზ ჩიხლაძე', 'ბიზნეს კომუნიკაცია(გაღრმავებული)', 25, 3),
(13, 'ლუკა ქუთათელაძე', 'სტარტაპი(გაღრმავებული)-2', 30, 4),
(14, 'ნათია მესტვირიშვილი', 'წუწუნინგი(გაღრმავებული)-2', 22, 4),
(15, 'ლერი სოხაძე', 'ბიზნეს კომუნიკაცია(გაღრმავებული)-2', 15, 4),
(16, 'ნიკოლოზ ჩიხლაძე', 'ბიზნეს საფუძვლები(გაღრმავებული)-2', 18, 4);

-- --------------------------------------------------------

--
-- Table structure for table `diplomatia`
--

CREATE TABLE `diplomatia` (
  `id` int(11) NOT NULL,
  `lector` varchar(100) NOT NULL,
  `subject` varchar(100) NOT NULL,
  `score` int(3) NOT NULL,
  `course` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `diplomatia`
--

INSERT INTO `diplomatia` (`id`, `lector`, `subject`, `score`, `course`) VALUES
(1, 'ნიკოლოზ ჩიხლაძე', 'დიპლომატია_1', 15, 1),
(2, 'ლუკა ქუთათელაძე', 'დიპლომატია_1', 22, 1),
(3, 'ნათია მესტვირიშვილი', 'დიპლომატია_1', 15, 1),
(4, 'ლერი სოხაძე', 'დიპლომატია_1', 22, 1),
(5, 'ლერი სოხაძე', 'დიპლომატია_2', 15, 2),
(6, 'ნათია მესტვირიშვილი', 'დიპლომატია_2', 22, 2),
(7, 'ლუკა ქუთათელაძე', 'დიპლომატია_2', 15, 2),
(8, 'ნიკოლოზ ჩიხლაძე', 'დიპლომატია_2', 22, 2),
(9, 'ნიკოლოზ ჩიხლაძე', 'დიპლომატია_3', 15, 3),
(10, 'ნათია მესტვირიშვილი', 'დიპლომატია_3', 22, 3),
(11, 'ლერი სოხაძე', 'დიპლომატია_3', 15, 3),
(12, 'ლუკა ქუთათელაძე', 'დიპლომატია_3', 22, 3),
(13, 'ლერი სოხაძე', 'დიპლომატია_4', 15, 4),
(14, 'ნიკოლოზ ჩიხლაძე', 'დიპლომატია_4', 22, 4),
(15, 'ლერი სოხაძე', 'დიპლომატია_4', 15, 4),
(16, 'ნათია მესტვირიშვილი', 'დიპლომატია_4', 22, 4);

-- --------------------------------------------------------

--
-- Table structure for table `filologia`
--

CREATE TABLE `filologia` (
  `id` int(11) NOT NULL,
  `lector` varchar(100) NOT NULL,
  `subject` varchar(100) NOT NULL,
  `score` int(3) NOT NULL,
  `course` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `filologia`
--

INSERT INTO `filologia` (`id`, `lector`, `subject`, `score`, `course`) VALUES
(1, 'ლუკა ქუთათელაძე', 'ფილოლოგია_1', 15, 1),
(2, 'ნიკოლოზ ჩიხლაძე', 'ფილოლოგია_1', 22, 1),
(3, 'ლერი სოხაძე', 'ფილოლოგია_1', 15, 1),
(4, 'ნათია მესტვირიშვილია', 'ფილოლოგია_1', 22, 1),
(5, 'ნიკოლოზ ჩიხლაძე', 'ფილოლოგია_2', 15, 2),
(6, 'ლუკა ქუთათელაძე', 'ფილოლოგია_2', 22, 2),
(7, 'ნათია მესტვირიშვილი', 'ფილოლოგია_2', 15, 2),
(8, 'ლერი სოხაძე', 'ფილოლოგია_2', 22, 2),
(9, 'ნათია მესტვირიშვილი', 'ფილოლოგია_3', 15, 3),
(10, 'ლუკა ქუთათელაძე', 'ფილოლოგია_3', 22, 3),
(11, 'ლერი სოხაძე', 'ფილოლოგია_3', 15, 3),
(12, 'ნიკოლოზ ჩიხლაძე', 'ფილოლოგია_3', 22, 3),
(13, 'ლერი სოხაძე', 'ფილოლოგია_4', 15, 4),
(14, 'ნიკოლოზ ჩიხლაძე', 'ფილოლოგია_4', 22, 4),
(15, 'ნათია მესტვირიშვილი', 'ფილოლოგია_4', 15, 4),
(16, 'ლუკა ქუთათელაძე', 'ფილოლოგია_4', 22, 4);

-- --------------------------------------------------------

--
-- Table structure for table `informatika`
--

CREATE TABLE `informatika` (
  `id` int(11) NOT NULL,
  `lector` varchar(100) NOT NULL,
  `subject` varchar(100) NOT NULL,
  `score` int(3) NOT NULL,
  `course` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `informatika`
--

INSERT INTO `informatika` (`id`, `lector`, `subject`, `score`, `course`) VALUES
(1, 'გვანცა წულაია ', 'C# დაპროგრამება (გაღრმავებული კურსი, ADO.NET)', 10, 3),
(2, 'ნიკოლოზ მაღლაფერიძე', 'გამოყენებითი  სტატისტიკა', 10, 3),
(3, 'ბესიკი ტაბატაძე', 'დაპროგრამების ენა Java', 15, 3),
(4, 'მირიან ყენია', '	\r\nკომპიუტერული გრაფიკა After Effect, level1', 14, 3),
(5, 'კონსტანტინე კულიჯანოვი', 'ლოკალური და გლობალური ქსელები', 0, 3),
(6, 'გულნარა კოტრიკაძე', 'ოპერაციული სისტემები', 5, 3),
(7, 'რუსუდან ახვლედიანი', '	\r\nრეკლამის ფსიქოლოგიის საფუძვლები (გამოყენებითი ფსიქოლოგია)', 15, 3),
(8, 'გვანცა წულაია ', 'C# დაპროგრამება (საფუძვლები)', 20, 2),
(9, 'ბესიკი ტაბატაძე', 'Java დაპროგრამება', 35, 2),
(10, 'ნიკოლოზ მაღლაფერიძე', 'ალბათობის თეორია', 15, 2),
(11, 'ბესიკი ტაბატაძე', 'ვებ ტექნოლოგიები (JAVA SCRIPT_jquery) Javascript frameworks -Angularjs', 24, 2),
(12, 'დინა  ასლამაზიშვილი', 'მოლაპარაკებანი - შესავალი', 10, 2),
(13, 'გვანცა წულაია ', 'მონაცემთა ბაზების მართვის სისტემები (SQL) (გაღრმავებული კურსი)', 28, 2),
(14, 'მარიამ ორკოდაშვილი', 'ტექნიკური ინგლისური IV', 17, 2),
(15, 'გვანცა წულაია ', 'C++ დაპროგრამება', 40, 1),
(16, 'დინა  ასლამაზიშვილი', 'ბიზნეს კავშირები-შესავალი', 35, 1),
(17, 'ნიკოლოზ მაღლაფერიძე', 'დისკრეტული მათემატიკა II', 36, 1),
(18, 'მირიან ყენია', '	\r\nვებ დიზაინის გრაფიკული საფუძვლები', 45, 1),
(19, 'ბესიკი ტაბატაძე', '	\r\nვებ ტექნოლოგიები (PHP_MYSQL)', 50, 1),
(20, 'ნატა ჭილაშვილი', '	\r\nტექნიკური ინგლისური II', 15, 4),
(21, 'ბესიკი ტაბატაძე', 'კომპიუტერული არქიტექტურა', 22, 4),
(22, 'მირიან ყენია', 'კომპიუტერული გრაფიკა II (Adobe Iluistrator)', 40, 4),
(23, 'ნიკოლოზ მაღლაფერიძე', 'წრფივი ალგებრა და ანალიზური გეომეტრია', 23, 4);

-- --------------------------------------------------------

--
-- Table structure for table `marks`
--

CREATE TABLE `marks` (
  `id` int(11) NOT NULL,
  `studentPin` varchar(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `firstMidterm` int(2) NOT NULL,
  `secondMidterm` int(2) NOT NULL,
  `final` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `marks`
--

INSERT INTO `marks` (`id`, `studentPin`, `title`, `firstMidterm`, `secondMidterm`, `final`) VALUES
(1, '01011081819', 'Java Programming', 20, 20, 60),
(2, '12312312312', 'C#(ASP.NET)', 19, 18, 59);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `pin` varchar(11) NOT NULL,
  `password` varchar(50) NOT NULL,
  `birthday` date NOT NULL,
  `gender` tinyint(1) NOT NULL,
  `faculty` varchar(50) NOT NULL,
  `course` int(1) NOT NULL,
  `permission` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `lastname`, `pin`, `password`, `birthday`, `gender`, `faculty`, `course`, `permission`) VALUES
(1, 'Nick', 'Chikhladze', '01011081819', 'root', '1998-05-06', 1, 'Informatics and Engineering', 3, 1),
(2, 'Mariam', 'Babisha', '12312312312', 'student', '1998-10-08', 0, 'Informatics and Engineering', 3, 0),
(3, 'Alexandree', 'Machavariani', '11111111111', 'student', '1998-10-08', 1, 'Law, Social Sciences and Diplomacy', 3, 0),
(4, 'Natia', 'Mestvirishvili', '11111111110', 'student', '1997-11-04', 0, 'Liberal Arts & Humanities', 4, 0),
(5, 'Qeti', 'Qetelauri', '11110111111', 'student', '1998-11-11', 0, 'Business', 2, 0),
(6, 'Ioane', 'Tandashvili', '01011121516', 'student', '2018-11-04', 1, 'Law, Social Sciences and Diplomacy', 2, 0),
(7, 'aaaa', 'asd', '1231231', 'asd', '2018-11-01', 1, 'Business', 1, 0),
(8, 'luka', 'qutateladze', '01019076023', 'Qutatela', '2016-11-17', 1, 'informatics', 3, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `appendence`
--
ALTER TABLE `appendence`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `biznesi`
--
ALTER TABLE `biznesi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `diplomatia`
--
ALTER TABLE `diplomatia`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `filologia`
--
ALTER TABLE `filologia`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `informatika`
--
ALTER TABLE `informatika`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `marks`
--
ALTER TABLE `marks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `pin` (`pin`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `appendence`
--
ALTER TABLE `appendence`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `biznesi`
--
ALTER TABLE `biznesi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `diplomatia`
--
ALTER TABLE `diplomatia`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `filologia`
--
ALTER TABLE `filologia`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `informatika`
--
ALTER TABLE `informatika`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `marks`
--
ALTER TABLE `marks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
